import { Controller, Get, Logger } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly logger: Logger,
  ) {}

  @Get()
  getHello(): string {
    this.logger.error('hello beam');
    const promise = new Promise((resolve, reject) => {
      reject();
    });
    promise.then(() => {
      this.logger.log('success');
    });
    return this.appService.getHello();
  }
}
