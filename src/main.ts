import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { WinstonModule } from 'nest-winston';
import * as winston from 'winston';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: WinstonModule.createLogger({
      transports: Object.assign(
        new winston.transports.Console({
          handleExceptions: true,
        }),
        {
          handleRejections: true,
        },
      ),
    }),
  });
  await app.listen(3000);
}
bootstrap();
